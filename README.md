# FEM-DVR-ECS DCS

Calculate differential cross section (DCS) for electron scattering  on simple spherically symmetrical potential.
Code is using Exterior complex scaling (ECS) and Finite difference- discrete variable representation functions (FEM-DVR) .